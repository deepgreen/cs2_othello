#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Board game;
    Side color;
    
    int heuristic(Board *board, int level, int alpha, int beta, Side side);
    int modScore(Side side, int i, int j);
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
