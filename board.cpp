#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
    
    //initial perimeter
    perimeter.set(2 + 8 * 2);
    perimeter.set(2 + 8 * 3);
    perimeter.set(2 + 8 * 4);
    perimeter.set(2 + 8 * 5);
    perimeter.set(3 + 8 * 5);
    perimeter.set(4 + 8 * 5);
    perimeter.set(5 + 8 * 5);
    perimeter.set(5 + 8 * 4);
    perimeter.set(5 + 8 * 3);
    perimeter.set(5 + 8 * 2);
    perimeter.set(4 + 8 * 2);
    perimeter.set(3 + 8 * 2);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    newBoard->perimeter = perimeter;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
    
    perimeter.reset(x + 8*y);
    // Set perimeter spots to 1.
    bool left = (x == 0);
    bool right = (x == 7);
    bool top = (y == 0);
    bool bottom = (y == 7);
    
    if (!left) {
        perimeter.set((x-1) + 8 * y);// left
        if (!bottom) {
            perimeter.set((x-1) + 8 * (y+1));// bottom left
        }
        if (!top) {
            perimeter.set((x-1) + 8 * (y-1));// top left
        }
    }
    if (!right) {
        perimeter.set((x+1) + 8 * y);// right
        if (!bottom) {
            perimeter.set((x+1) + 8 * (y+1));// bottom right
        }
        if (!top) {
            perimeter.set((x+1) + 8 * (y-1));// top right
        }
    }
    if (!top) {
        perimeter.set(x + 8 * (y-1));// top
    }
    if (!bottom) {
        perimeter.set(x + 8 * (y+1));// bottom
    }
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;
    
    // Make sure non-perimeter squares are not considered.
    if (!perimeter[X + 8*Y]) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

bool Board::edgeSafe(Side side, int i, int j) {
    Side other = (side == BLACK) ? WHITE : BLACK;
    bool left = (i == 0);
    bool right = (i == 7);
    bool top = (j == 0);
    bool bottom = (j == 7);
    if (left or right) {
        if (get(other, i, j-1)) {
            return !get(other, i, j+1);
        }
        else if (get(other, i, j+1)) {
            return !get(other, i, j-1);
        }
        else return true;    
    }
    else if (top or bottom) {
        if (get(other, i-1, j)) {
            return !get(other, i+1, j);
        }
        else if (get(other, i+1, j)) {
            return !get(other, i-1, j);
        }
        else return true;
    }
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}
