#include "exampleplayer.h"

#define MAX_LEVEL 7           // lvl 7 has 677 seconds left. lvl 6 w/ 861.
#define SCALE 8
#define INFINITY 1000

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     game = Board();
     color = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


int ExamplePlayer::heuristic(Board * board, int level, int alpha, int beta, Side side)
{
    //std::cerr << level << std::endl;
    Side other = (side == BLACK) ? WHITE : BLACK;
    int h_number;
       
    if (level == MAX_LEVEL || board->isDone())
    {// Reached maximum depth of path.
        Side opponent = (color == BLACK) ? WHITE : BLACK;
        return board->count(color) - board->count(opponent);
    }
    if (!board->hasMoves(other))
    {// Check for existing nodes.
        return heuristic(board, level+1, alpha, beta, other);
    }
    
    int i, j;
    if (side == color) {
        for (i=0; i < 8; i++)
        {// Iterate through possible nodes.
            for (j=0; j < 8; j++) {
                Move oth_temp(i, j);
                if (board->checkMove(&oth_temp, other))
                {// If node move exists
                    Board *board_copy = board->copy();
                    board_copy->doMove(&oth_temp, other);
                    h_number = heuristic(board_copy, level+1, \
                                         alpha, beta, other);
                    h_number += modScore(other, i, j);
                    beta = std::min(beta, h_number);
                    delete board_copy;
                    if (beta <= alpha)
                    {// Prune path
                        return beta;
                    }
                }
            }
        }
        return beta;
    } 
    else {
        for (i=0; i < 8; i++)
        {// Iterate through possible nodes
            for (j=0; j < 8; j++) {
                Move oth_temp(i, j);
                if (board->checkMove(&oth_temp, other))
                {// If node move exists
                    Board *board_copy = board->copy();
                    board_copy->doMove(&oth_temp, other);
                    h_number = heuristic(board_copy, level+1, \
                                         alpha, beta, other);
                    h_number += modScore(other, i, j);
                    alpha = std::max(alpha, h_number);
                    delete board_copy;
                    if (beta <= alpha)
                    {// Prune path
                        return alpha;
                    }
                }
            }
        }
        return alpha;
    }
}

int ExamplePlayer::modScore(Side side, int i, int j)
{
    Side other = (side == BLACK) ? WHITE : BLACK;
    int res = 0;
    if (((i == 0) || (i == 7)) && ((j == 0) || (j == 7)))
    {// Corner move.
        res = 3*SCALE;
    }
    else if (((i == 1) && (j < 6) && (j > 1)) || \
             ((i == 6) && (j < 6) && (j > 1)) || \
             ((j == 1) && (i < 6) && (i > 1)) || \
             ((j == 6) && (i < 6) && (i > 1)))
    {// Next-to-edge move.
        res = -2*SCALE;
    }
    else if (((i > 5) || (i < 2)) && ((j > 5) || (j < 2)))
    {//Next to corner move.
        res = -4*SCALE;
    }
    else if ((i == 0) || (i == 7) || (j == 0) || (j == 7))
    {
        if (game.edgeSafe(side, i, j)) res = SCALE;
        else res = SCALE;
    }
    if (side != color) {
        res *= -1;
    }
    return res;
}   

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    //std::cerr << "thinking about best move" << std::endl;
    cerr.flush();
    // Make opponenets move.
    Side other = (color == BLACK) ? WHITE : BLACK;
    game.doMove(opponentsMove, other);
    
    if (!game.hasMoves(color)) return NULL;
    
    int max = -INFINITY;
    int h_number;
    Move *max_move = NULL;
    for (int i = 0; i < 8; i++)
    {// Iterate through each board square to check possible moves.
        for (int j = 0; j < 8; j++) 
        {
            Move *temp = new Move(i, j);
            if (game.checkMove(temp, color))
            {
                Board *board_copy = game.copy();
                board_copy->doMove(temp, color);
                
                h_number = heuristic(board_copy, 0, -INFINITY, INFINITY, color);
               // std::cerr << "score:  " << h_number << "  for: " << i << " " << j << std::endl;
                h_number += modScore(color, i, j);
                //std::cerr << "after:   " << h_number << std::endl;
                delete board_copy;
                if (h_number > max)
                {
                    max = h_number;
                    max_move = temp;
                }
            }
        }
    }
    std::cerr << "Done thinking, picked(" << max_move->getX() << "," << \
                max_move->getY() << ") " << max << " time left: " << \ 
                msLeft/1000 << " s" << std::endl;
    game.doMove(max_move, color);
    return max_move;                    
}
    
