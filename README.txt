README file for CS2 Othello Team Deep Green

Alex Barreiro worked on the heuristic, alpha-beta pruning, score
modification, and "safe move" check.

Ian Garcia worked on the heuristic, alpha-beta pruning, score
modification, and board perimeter.

Our AI implements alpha-beta pruning, perimeter confining, 
score mods, and safe-move checking to allow itself to
think far enough ahead for the most intelligent move.